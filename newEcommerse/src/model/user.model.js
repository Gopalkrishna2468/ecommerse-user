const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const userSchema = new Schema({
    name: {
        type: String,
      },
      email: {
        type: String,
        unique:true
      },
      role: {
        type: String,
        default: "user",
        enum: ["user", "admin"],
      },
      hashed_password: {
        type: String,
      },
      salt: String,
      updated: Date,
      created: {
        type: Date,
        default: Date.now,
      }
})


userSchema
  .virtual('password')//set virtual property password
  .set(function (password) {
    this._password = password //this field is not stored in DB
    this.salt = this.makeSalt()
    this.hashed_password = this.encryptPassword(password)
  })
  .get(function () {
    return this._password
  })
userSchema.methods = {
  authenticate: function (plainText) {
    return this.encryptPassword(plainText) === this.hashed_password
  },
  encryptPassword: function (password) {
    if (!password) return ''
    try {
      return crypto
        .createHmac('sha1', this.salt)
        .update(password)
        .digest('hex')
    } catch (err) {
      return ''
    }
  },
  makeSalt: function () {
    return Math.round((new Date().valueOf() * Math.random())) + ''
  }
}

var User = mongoose.model('User', userSchema);
module.exports = User