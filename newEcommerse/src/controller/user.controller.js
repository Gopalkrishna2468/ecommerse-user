const User = require('../model/user.model')

module.exports.addUser = async(req,res) =>{
    try{
        const newUser = new User(req.body)
        const data = await newUser.save()
        res.send({data})
    }catch(err){
        console.log(err)
    }   
}

module.exports.getAllUsers = async (req, res) => {
    
    try {
      const data = await User.find()
      res.send({data})
    }
    catch (err) {
      console.log(err);
         }
  }

module.exports.getUserById = async(req,res) =>{
    try{
        const id = req.params.id
        const data = await User.findOne({_id : id}, req.body)
        res.send({data})
    }catch(err){
        console.log(err)
    }   
}

module.exports.updateUser = async(req,res) =>{
    try{
        const id = req.params.id
        const data = await User.updateOne({_id : id}, req.body)
        res.send({data})
    }catch(err){
        console.log(err)
    }   
}


module.exports.deleteUser = async (req, res) => {
    
    try {
        const id = req.params.id
      const deletedUser = await User.findByIdAndDelete({ _id:id }, req.body)
      res.send({deletedUser})
    }
    catch (err) {
      console.log(err);
         }
  }