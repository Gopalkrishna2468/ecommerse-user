const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
const DB = 'mongodb+srv://Thapa:Thisroot@cluster0.ukwbq.mongodb.net/UsersData?retryWrites=true&w=majority'
const userRouter = require('./src/route/user.route')

mongoose.connect(DB, {
    useNewUrlParser: true,
    useUnifiedTopology:true,
}).then(() =>{
    console.log('connected mongo')
}).catch((err) =>{
    console.log('Error' +err)
})

const app = express()

app.use(cors())
app.use(express.json())


app.use('/user', userRouter)

app.listen(3500, () =>console.log('Listining to port 3500'))